# sites-élèves

⚠️ Avant de plublier des travaux d'élèves, il faut faire remplir par leurs responsables légaux une demande d'autorisation (droit d'auteur). Voici un exemple :

Ce dépot porte la publication de pages internet rédigées par des élèves, notamment en SNT avec l'application WEB de Capytale.fr  
Sur le site publié, une page d'accueil affiche des liens vers chacun des groupes  
Sur la page de chagque groupe s'affiche des liens vers chacun des projets des élèves.

### Dans capytale
- Sur votre ordinateur créer un dossier au nom de chacun des groupes de SNT
- Dans Capytale, télécharger un à un les pages web de vos élèves au format **zip** dans ces dossiers

![capytale](capytale.png)

### Dans la Forge
- Créer une bifurcation de ce projet

![forge0](forge0.png)

- Editer le contenu du projet

![forge1](forge1.png)

- Supprimer les dossiers qu'il contient
- Ajouter les dossiers de vos groupes de SNT contenant les fichiers zip  

![forge2](forge2.png)

- Valider les modifications  

![forge3](forge3.png)

